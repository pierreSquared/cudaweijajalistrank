# README #

A CUDA implementation of List Ranking algorithm by Wei and JaJa.

Based on:

"Optimization of linked list prefix computations on multithreaded GPUs using CUDA"

Zheng Wei and Joseph JaJa

Published in: 2010 IEEE International Symposium on Parallel & Distributed Processing (IPDPS)

19-23 April 2010,

DOI: https://doi.org/10.1109/IPDPS.2010.5470455

### Requirements ###
* Moderngpu 2.0: https://github.com/moderngpu/moderngpu