#include <moderngpu/transform.hxx>

using namespace mgpu;

// Head equals to 0, root to N-1
void cudaWeiJaJaListRank( int* devRank, int N, int head, int* devNext, standard_context_t& context );